/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_negative_power.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/15 19:21:36 by dfarhi            #+#    #+#             */
/*   Updated: 2022/07/15 19:21:46 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expanded.h"

double	ft_negative_power(int nb, int power)
{
	return ((double)1 / ft_power(nb, power));
}
