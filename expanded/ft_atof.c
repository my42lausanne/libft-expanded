/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/15 19:22:16 by dfarhi            #+#    #+#             */
/*   Updated: 2022/07/15 19:22:32 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "expanded.h"

static int	get_level(const char *str)
{
	int	i;
	int	level;
	int	dot;

	level = 0;
	i = -1;
	dot = 0;
	while (ft_isdigit(str[++i]) || str[i] == '.')
	{
		if (str[i] == '.')
		{
			if (dot > 0)
				break ;
			else
			{
				dot++;
				continue ;
			}
		}
		level++;
	}
	if (level > 0)
		level--;
	return (level);
}

static int	skip_chars(int *index, const char *str)
{
	while (ft_isspace(str[*index + 1]))
		*index = *index + 1;
	if (str[*index + 1] == '-')
	{
		*index = *index + 1;
		return (-1);
	}
	if (str[*index + 1] == '+')
		*index = *index + 1;
	return (1);
}

static double	atof_loop(const char *str, int i, unsigned int level)
{
	int				dot;
	unsigned long	n;

	n = 0;
	dot = 0;
	while (ft_isdigit(str[++i]) || str[i] == '.')
	{
		if (str[i] == '.')
		{
			if (dot > 0)
				break ;
			else
			{
				dot++;
				continue ;
			}
		}
		if (dot)
			dot++;
		n += (str[i] - '0') * ft_power(10, level--);
	}
	if (dot > 0)
		dot--;
	return ((double)n * ft_negative_power(10, dot));
}

double	ft_atof(const char *str)
{
	int				i;
	int				negative;
	unsigned int	level;

	i = -1;
	negative = skip_chars(&i, str);
	level = get_level(&str[i + 1]);
	return ((double)negative * atof_loop(str, i, level));
}
